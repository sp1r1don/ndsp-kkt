import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:vilka_app/views/login.dart';

class RegistrationResult extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.all(32),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(bottom: 32),
              width: double.infinity,
              child: Text(
                'Registration\nSuccessful',
                textAlign: TextAlign.center,
                style: GoogleFonts.inter(
                  fontSize: 28.0,
                  fontWeight: FontWeight.w600,
                  color: Color(0xff0A0E31),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 32),
              child: Image.asset(
                'assets/images/ic_register_success.png',
              ),
            ),
            Container(
              width: double.infinity,
              margin: EdgeInsets.only(bottom: 32),
              child: Text(
                'Your data was checked\nby Tax Service successfully.\nYou can start working right now!',
                textAlign: TextAlign.center,
                style: GoogleFonts.inter(
                  fontSize: 16.0,
                  color: Color(0x880A0E31),
                  height: 1.5,
                ),
              ),
            ),
            Container(
              height: 48,
              width: double.infinity,
              child: FlatButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(4)
                ),
                color: Color(0xff00b283),
                textColor: Color(0xffffffff),
                highlightColor: Color(0x5500b283),
                disabledColor: Color(0x5500b283),
                onPressed: () => Navigator.of(context).push(MaterialPageRoute(
                  builder: (_) => Login(),
                )),
                child: Text(
                  'Login',
                  style: GoogleFonts.inter(
                    fontSize: 16.0,
                    color: Color(0xffffffff),
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
