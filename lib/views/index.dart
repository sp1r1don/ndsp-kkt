import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:vilka_app/views/activation.dart';

class Index extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: double.infinity,
            child: Text(
              'Welcome!',
              textAlign: TextAlign.center,
              style: GoogleFonts.inter(
                fontSize: 28.0,
                fontWeight: FontWeight.w600,
                color: Color(0xff0A0E31),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(0, 20, 0, 40),
            width: 280,
            child: Text(
              'Connect your fiscal memory \n device and hold near the phone!',
              textAlign: TextAlign.center,
              style: GoogleFonts.inter(
                fontSize: 16.0,
                color: Color(0x880A0E31),
                height: 1.5,
              ),
            ),
          ),
          Container(
            child: Image.asset(
              'assets/images/connect_card.png',
              width: 280,
              fit: BoxFit.contain,
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 40),
            child: Text(
              'or',
              style: GoogleFonts.inter(
                fontSize: 16.0,
                color: Color(0x880A0E31),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 40),
            height: 48,
            child: OutlineButton(
              highlightColor: Color(0x5500b283),
              borderSide: BorderSide(
                color: Color(0xff00b283),
              ),
              onPressed: () => Navigator.of(context).push(MaterialPageRoute(
                builder: (_) {
                  return Activation();
                }
              )),
              child: Text(
                'Use Cloud Cash Register',
                style: GoogleFonts.inter(
                  fontSize: 16.0,
                  color: Color(0xff00b283),
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}