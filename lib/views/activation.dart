import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:vilka_app/views/address.dart';

class Activation extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.all(32),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              flex: 1,
              child: Column(
                children: [
                  Container(
                    width: double.infinity,
                    child: Text(
                      'Activation',
                      textAlign: TextAlign.left,
                      style: GoogleFonts.inter(
                        fontSize: 28.0,
                        fontWeight: FontWeight.w600,
                        color: Color(0xff0A0E31),
                      ),
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.fromLTRB(0, 25, 0, 15),
                    child: Text(
                      'Enter PIN',
                      textAlign: TextAlign.left,
                      style: GoogleFonts.inter(
                        fontSize: 16.0,
                        color: Color(0x880A0E31),
                        height: 1.5,
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: 66,
                        child: TextField(
                          style: GoogleFonts.inter(
                            fontSize: 16.0,
                            color: Color(0xff121010),
                          ),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderSide: BorderSide(color: Color(0xffe2e2e6)),
                            ),
                          ),
                          autofocus: true,
                          keyboardType: TextInputType.number,
                        ),
                      ),
                      Container(
                        width: 66,
                        child: TextField(
                          style: GoogleFonts.inter(
                            fontSize: 16.0,
                            color: Color(0xff121010),
                          ),
                          autofocus: true,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderSide: BorderSide(color: Color(0xffe2e2e6)),
                            ),
                          ),
                          keyboardType: TextInputType.number,
                        ),
                      ),
                      Container(
                        width: 66,
                        child: TextField(
                          style: GoogleFonts.inter(
                            fontSize: 16.0,
                            color: Color(0xff121010),
                          ),
                          autofocus: true,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderSide: BorderSide(color: Color(0xffe2e2e6)),
                            ),
                          ),
                          keyboardType: TextInputType.number,
                        ),
                      ),
                      Container(
                        width: 66,
                        child: TextField(
                          style: GoogleFonts.inter(
                            fontSize: 16.0,
                            color: Color(0xff121010),
                          ),
                          autofocus: true,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderSide: BorderSide(color: Color(0xffe2e2e6)),
                            ),
                          ),
                          keyboardType: TextInputType.number,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              height: 48,
              width: double.infinity,
              margin: EdgeInsets.only(top: 40),
              child: FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4)
                ),
                color: Color(0xff00b283),
                textColor: Color(0xffffffff),
                highlightColor: Color(0x5500b283),
                disabledColor: Color(0x5500b283),
                onPressed: () => Navigator.of(context).push(MaterialPageRoute(
                  builder: (_) => Address(),
                )),
                child: Text(
                  'Next',
                  style: GoogleFonts.inter(
                    fontSize: 16.0,
                    color: Color(0xffffffff),
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
