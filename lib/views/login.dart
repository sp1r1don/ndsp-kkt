import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Login extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.all(32.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.only(bottom: 72),
              width: double.infinity,
              child: Text(
                'Sign In',
                textAlign: TextAlign.center,
                style: GoogleFonts.inter(
                  fontSize: 28.0,
                  fontWeight: FontWeight.w600,
                  color: Color(0xff0A0E31),
                ),
              ),
            ),
            Container(
              width: double.infinity,
              height: 56,
              margin: EdgeInsets.only(bottom: 25),
              child: TextField(
                style: GoogleFonts.inter(
                  fontSize: 16.0,
                  color: Color(0xff121010),
                ),
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide(color: Color(0xffe2e2e6)),
                    ),
                    hintText: 'Login'
                ),
              ),
            ),
            Container(
              width: double.infinity,
              height: 56,
              margin: EdgeInsets.only(bottom: 25),
              child: TextField(
                style: GoogleFonts.inter(
                  fontSize: 16.0,
                  color: Color(0xff121010),
                ),
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide(color: Color(0xffe2e2e6)),
                    ),
                    hintText: 'Password'
                ),
              ),
            ),
            Container(
              height: 48,
              width: double.infinity,
              margin: EdgeInsets.only(bottom: 25),
              child: FlatButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(4)
                ),
                color: Color(0xff00b283),
                textColor: Color(0xffffffff),
                highlightColor: Color(0x5500b283),
                disabledColor: Color(0x5500b283),
                onPressed: () => Navigator.of(context).push(MaterialPageRoute(
                  builder: (_) => Login(),
                )),
                child: Text(
                  'Login',
                  style: GoogleFonts.inter(
                    fontSize: 16.0,
                    color: Color(0xffffffff),
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
            Container(
              width: double.infinity,
              height: 48,
              child: OutlineButton(
                highlightColor: Color(0x0000b283),
                focusColor: Color(0x0000b283),
                borderSide: BorderSide(
                  color: Color(0x0000b283),
                ),
                onPressed: () => true,
                child: Text(
                  'Forgot Password?',
                  style: GoogleFonts.inter(
                    fontSize: 16.0,
                    color: Color(0xff00b283),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
