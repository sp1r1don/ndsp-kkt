import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:vilka_app/views/registration_result.dart';

class Address extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              child: Container(
                margin: EdgeInsets.all(32),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.only(bottom: 25),
                      width: double.infinity,
                      child: Text(
                        'Address',
                        textAlign: TextAlign.left,
                        style: GoogleFonts.inter(
                          fontSize: 28.0,
                          fontWeight: FontWeight.w600,
                          color: Color(0xff0A0E31),
                        ),
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      margin: EdgeInsets.only(bottom: 20),
                      child: Text(
                        'Please enter store name and cash\nregister address',
                        textAlign: TextAlign.left,
                        style: GoogleFonts.inter(
                          fontSize: 16.0,
                          color: Color(0x880A0E31),
                          height: 1.5,
                        ),
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      height: 56,
                      margin: EdgeInsets.only(bottom: 30),
                      child: TextField(
                        style: GoogleFonts.inter(
                          fontSize: 16.0,
                          color: Color(0xff121010),
                        ),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xffe2e2e6)),
                          ),
                          hintText: 'Store Name'
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 30),
                      child: Row(
                        children: [
                          InkWell(
                            onTap: null,
                            child: Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(right: 10),
                                    width: 20,
                                    height: 20,
                                    child: Checkbox(
                                        value: true,
                                        tristate: false,
                                        onChanged: (bool value) {
                                          print(value);
                                          print('hello');
                                        },
                                        activeColor: Color(0xff00b283)
                                    ),
                                  ),
                                  Text(
                                    'Itinerant Trade',
                                    style: GoogleFonts.inter(
                                      fontSize: 16.0,
                                      color: Color(0xff121010),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      )
                    ),
                    Container(
                      width: double.infinity,
                      margin: EdgeInsets.only(bottom: 5),
                      child: Text(
                        'Address',
                        textAlign: TextAlign.left,
                        style: GoogleFonts.inter(
                          fontSize: 13.0,
                          color: Color(0x880A0E31),
                          height: 1.5,
                        ),
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      margin: EdgeInsets.only(bottom: 20),
                      child: Text(
                        'Orozbekov St, 9, Bishkek, 720033',
                        textAlign: TextAlign.left,
                        style: GoogleFonts.inter(
                          fontSize: 16.0,
                          color: Color(0xff121010),
                          height: 1.5,
                        ),
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      height: 48,
                      child: OutlineButton(
                        highlightColor: Color(0x5500b283),
                        borderSide: BorderSide(
                          color: Color(0xff00b283),
                        ),
                        onPressed: () => true,
                        child: Text(
                          'Choose Address',
                          style: GoogleFonts.inter(
                            fontSize: 16.0,
                            color: Color(0xff00b283),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ),
            Container(
              padding: EdgeInsets.fromLTRB(32, 5, 32, 35),
              width: double.infinity,
              decoration: BoxDecoration(
                border: Border(
                  top: BorderSide(
                    color: Color(0xffe2e2e6),
                    width: 1
                  )
                )
              ),
              child: Container(
                height: 48,
                width: double.infinity,
                child: FlatButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(4)
                  ),
                  color: Color(0xff00b283),
                  textColor: Color(0xffffffff),
                  highlightColor: Color(0x5500b283),
                  disabledColor: Color(0x5500b283),
                  onPressed: () => Navigator.of(context).push(MaterialPageRoute(
                    builder: (_) => RegistrationResult(),
                  )),
                  child: Text(
                    'Register',
                    style: GoogleFonts.inter(
                      fontSize: 16.0,
                      color: Color(0xffffffff),
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              )
            )
          ],
        ),
      ),
    );
  }
}
